# greenCreative Core

本仓库装载有greenCreative的预览版核心。

## 安装

* 安装 Git（比如 Git for Windows 用于 Windows 系统）。请您自行搜索教程。
* 打开您的存档文件夹的 `datapacks` 目录。
* 克隆本存储库，e.g. `git clone https://gitlab.com/RelaperDev/greencreative-core.git`
* 如果您的游戏在运行中，重新打开您的存档
* 运行指令 `/tag @s add grant` 和 `/scoreboard players set @s flag 4`（设置自己为管理员）

### 更新

* 导航到您的存档文件夹下的 `datapacks` 目录中的 `greencreative-core` 下
* 运行 `git pull`
* 如果您的游戏在运行中，重新打开您的存档（或者运行 `/reload`）

## 许可证

本数据包依照 GNU Affero General Public License 3.0（或其以后版本）授权。简而言之——

* 如果您使用的本数据包未被您作修改，您需要在您的作品的**可见**（可以**相对**不太显眼，但必须可以看到，如主城的“开源许可证”按钮中或者欢迎消息中）部分添加本项目的开源链接。
* 如果您使用的本数据包被您作了修改，您需要以同样许可证将其开源，并在您的作品的**可见**（可以**相对**不太显眼，但必须可以看到，如主城的“开源许可证”按钮中或者欢迎消息中）部分添加您的数据包的开源链接。
