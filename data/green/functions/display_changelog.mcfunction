# tellraw @p ["============== 更 新 日 志 =============="]
# tellraw @p [{"text": "大厅版本21-","color": "gold"},{"text":"Week27-Build.2-alpha1 2021/7/11","bold": true,"color": "red","hoverEvent": {"action": "show_text","value": "代表2021年7月11日发布的版本，也是第27周发布的第3个版本。\n在非正式的情况下，您可以遵循网易平台开发者“风云迈啊密”的版本命名措施作为简写，\n即“A03.09”，但在此版本需同时称其Alpha版本，即“A03.09-alpha”。"}},{"text": " | ","bold": false,"color": "white"},{"text": "Alpha版","color": "green","bold": false,"italic": true,"hoverEvent": {"action": "show_text","value": "Alpha版本表示有重大改动或者架构改动的版本，仅经过单机测试。\n对于普通玩家来说，此版本相对稳定，但不排除有重大问题。\n此阶段会快速增加大量功能。"}}]
# tellraw @p {"text": "- 危险用户、普通违禁品（清空）、危险实体、新人审核功能移动至函数","color": "aqua","bold": false}
# tellraw @p {"text": "- 将部分功能预载入函数系统","color": "aqua"}
# tellraw @p {"text": "- 现在使用物品类型标签（非NBT）追踪违禁品","color": "aqua"}
# tellraw @p {"text": "- 现在使用实体类型标签（非NBT）追踪危险实体","color": "aqua"}
# tellraw @p {"text": "- 现在站在被至少一个品红色带釉陶瓦指向的铁块上能够获得速度II效果","color": "aqua"}
# tellraw @p {"text": "- 现在站在绿宝石块上能够获得跳跃提升II效果","color": "aqua"}
# tellraw @p {"text": "- 信息板侧边栏文本显示效果增强","color": "aqua"}
# tellraw @p {"text": "- 信息板侧边栏增加当前Tab列表展示的分数的类别","color": "aqua"}
# tellraw @p {"text": "- 信息板侧边栏增加当前在线的管理数量","color": "aqua"}
# tellraw @p {"text": "- 增加阈值扫地机，目前设为15个掉落物清理一次","color": "aqua"}
# tellraw @p {"text": "- 熔岩桶、末地门框架和末影之眼移出增强掉落物，现在只会被清空了","color": "aqua"}
# tellraw @p {"text": "- 增加一个管理员触发器 \"remove_pillager\"","color": "aqua"}
# tellraw @p {"text": "- 权限等级实装，目前用于控制管理员触发器的使用权限","color": "aqua"}
# tellraw @p {"text": "- 办事处增加全房点歌机，目前只支持Pigstep","color": "aqua"}
# tellraw @p {"text": "- 封禁名单移动至主城公告箱子内","color": "aqua"}
# tellraw @p " -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- "
# tellraw @p [{"text": "大厅版本21-","color": "gold"},{"text":"Week27-Build.2-alpha2 2021/7/11","bold": true,"color": "red","hoverEvent": {"action": "show_text","value": "代表2021年7月11日发布的版本，也是第27周发布的第4个版本。\n在非正式的情况下，您可以遵循网易平台开发者“风云迈啊密”的版本命名措施作为简写，\n即“A03.10”，但在此版本需同时称其Alpha版本，即“A03.10-alpha”。"}},{"text": " | ","bold": false,"color": "white"},{"text": "Alpha版","color": "green","bold": false,"italic": true,"hoverEvent": {"action": "show_text","value": "Alpha版本表示有重大改动或者架构改动的版本，仅经过单机测试。\n对于普通玩家来说，此版本相对稳定，但不排除有重大问题。\n此阶段会快速增加大量功能。"}}]
# tellraw @p {"text": "- 增加数个新的触发器，用于封禁、设置为星辰竹鼠、解除星辰竹鼠","color": "aqua","bold": false}
# tellraw @p {"text": "若发现任何Bug请私信（游戏内或启动器）房主反馈问题（游戏内私信模板点击<报告问题>使用）","color": "gray","italic": true}
# tellraw @p [{"text":"[已知问题]","color":"gray","strikethrough": true}," ",{"text": "[报告问题]","color": "green","strikethrough": false,"clickEvent": {"action": "suggest_command","value": "/msg RelapCrystal \"BUG\": [<替换为BUG的描述>] (-currentVer)"}}]

tellraw @p [{"text": "[点我直达开源项目]","color": "green","hoverEvent": {"action": "show_text","value": "<链接>"},"clickEvent": {"action": "open_url","value": "https://gitlab.com/RelaperDev/greencreative-core/"}}]