clear @a[tag=!grant] #green:forbidden_a
clear @a[scores={flag=..0}] minecraft:slime_block
clear @a[scores={flag=..0}] minecraft:sticky_piston
clear @a[scores={flag=..0}] minecraft:observer
clear @a piston{BlockStateTag:{extended:"true"}}

execute in the_nether run clear @a[distance=0..,tag=!grant] #minecraft:beds
execute in minecraft:overworld run clear @a[distance=0..,tag=!grant] respawn_anchor
effect clear @a[tag=noresist] resistance