scoreboard players set @s cache 1350
execute as @a run execute if score @s uid = @a[scores={cache=1350,flag=..2},limit=1] block run scoreboard players set @s cache 1370
execute as @a[scores={cache=1370}] run execute if entity @s[scores={flag=3..}] run tellraw @a[scores={cache=1350}] {"text": "您没有权限封禁此用户","color": "red"}
execute as @a[scores={cache=1370}] run execute if entity @s[tag=grant] run tellraw @a[scores={cache=1350}] {"text": "您没有权限封禁此用户","color": "red"}
execute unless entity @a[scores={cache=1370}] run tellraw @s {"text":"该UID没有对应的用户，或者对应的用户不在线，或者您不能封禁此用户","color":"red"}
execute if entity @a[scores={cache=1370},tag=banned] run tellraw @s {"text":"该用户当前在封禁中，不能重复封禁","color":"red"}
execute if entity @a[scores={cache=1370},tag=banned2] run tellraw @s {"text":"该用户当前在封禁中，不能重复封禁","color":"red"}
execute if entity @a[scores={cache=1370},tag=!grant,tag=!banned,tag=!banned2] run tag @a[scores={cache=1370,flag=..2},tag=!grant,tag=!banned,tag=!banned2,limit=1] add banned2
execute if entity @a[scores={cache=1370},tag=banned2] run tellraw @s "用户已封禁"
scoreboard players set @a[scores={cache=1370}] cache 0
scoreboard players set @s block 0
scoreboard players set @s cache 0