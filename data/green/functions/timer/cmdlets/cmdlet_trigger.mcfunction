# Declare it as per syntax highlight requirements
scoreboard objectives add remove_pillagers trigger
scoreboard objectives add query_player trigger
scoreboard objectives add help trigger
scoreboard objectives add block trigger
scoreboard objectives add make_mouse trigger
scoreboard objectives add unmouse trigger
scoreboard objectives add spawn trigger
scoreboard objectives add version trigger

execute as @a[scores={remove_pillagers=1..}] run function green:timer/cmdlets/remove_pillager
execute as @a[scores={query_player=1..}] run function green:timer/cmdlets/query_player
execute as @a[scores={help=1..}] run function green:timer/cmdlets/help
execute as @a[scores={block=1..}] run function green:timer/cmdlets/block
execute as @a[scores={make_mouse=1..}] run function green:timer/cmdlets/make_mouse
execute as @a[scores={unmouse=1..}] run function green:timer/cmdlets/unmouse
execute as @a[scores={spawn=1..}] run function green:timer/cmdlets/spawn
execute as @a[scores={version=1..}] run function green:timer/cmdlets/version