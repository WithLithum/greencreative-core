tellraw @s "所有触发器指令："
tellraw @s " -=-=-=-=-=-=-=-=-=-=- "
tellraw @s [{"text":"version","color": "green"},{"text": " - 检查运行的软件版本及开源地址","color": "white"}]
tellraw @s ["分数设置：任意 | ",{"text": "任意权限","color": "red","italic": true}]
tellraw @s " -=-=-=-=-=-=-=-=-=-=- "
tellraw @s [{"text":"query_player","color": "green"},{"text": " - 检查UID属于哪个玩家","color": "white"}]
tellraw @s ["分数设置：UID | ",{"text": "需要至少3级权限","color": "red","italic": true}]
tellraw @s " -=-=-=-=-=-=-=-=-=-=- "
tellraw @s [{"text":"remove_pillager","color": "green"},{"text": " - 击杀所有掠夺者","color": "white"}]
tellraw @s ["分数设置：任意 | ",{"text": "需要至少3级权限","color": "red","italic": true}]
tellraw @s " -=-=-=-=-=-=-=-=-=-=- "
tellraw @s [{"text":"block","color": "green"},{"text": " - 封禁","color": "white"}]
tellraw @s ["分数设置：UID | ",{"text": "需要至少4级权限","color": "red","italic": true}]
tellraw @s " -=-=-=-=-=-=-=-=-=-=- "
tellraw @s [{"text":"make_mouse","color": "green"},{"text": " - 设置为星辰竹鼠","color": "white"}]
tellraw @s ["分数设置：UID | ",{"text": "需要至少3级权限","color": "red","italic": true}]
tellraw @s " -=-=-=-=-=-=-=-=-=-=- "
tellraw @s [{"text":"unmouse","color": "green"},{"text": " - 解除烤老鼠","color": "white"}]
tellraw @s ["分数设置：UID | ",{"text": "需要至少3级权限","color": "red","italic": true}]
tellraw @s " -=-=-=-=-=-=-=-=-=-=- "
tellraw @s [{"text":"spawn","color": "green"},{"text": " - 回到主城","color": "white"}]
tellraw @s ["分数设置：任意 | ",{"text": "任意权限","color": "red","italic": true}]
tellraw @s " -=-=-=-=-=-=-=-=-=-=- "
scoreboard players set @s help 0