scoreboard players set @s cache 1300
execute as @a run execute if score @s uid = @a[scores={cache=1300,flag=3..},limit=1] query_player run scoreboard players set @s cache 1250
execute if entity @a[scores={cache=1250}] run tellraw @s ["该UID所属用户为:",{"selector":"@a[scores={cache=1250}]"}]
execute unless entity @a[scores={cache=1250}] run tellraw @s {"text":"该UID没有对应的用户，或者对应的用户不在线","color":"red"}
scoreboard players set @a[scores={cache=1250}] cache 0
scoreboard players set @s query_player 0
scoreboard players set @s cache 0