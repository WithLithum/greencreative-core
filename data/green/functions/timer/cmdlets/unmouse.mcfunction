scoreboard players set @s cache 1442
execute as @a[tag=rat] run execute if score @s uid = @a[scores={cache=1442},limit=1] unmouse run scoreboard players set @s cache 1443
execute unless entity @a[scores={cache=1443}] run tellraw @s {"text":"该UID没有对应的用户，或者对应的用户不在线，或者不是星辰竹鼠","color":"red"}
execute if entity @a[scores={cache=1443}] run tag @a[scores={cache=1443}] remove rat
execute as @a[scores={cache=1443}] run execute in minecraft:overworld run tp -270 70 242
scoreboard players set @a[scores={cache=1443}] cache 0
execute if entity @a[scores={cache=1443},tag=!rat] run tellraw @s "已解除星辰竹鼠标记"
scoreboard players set @s unmouse 0
scoreboard players set @s cache 0