scoreboard players add @a playtime 1
execute as @a[scores={playtime=600..},advancements={green:creative/just_here=false}] run advancement grant @s only green:creative/just_here
execute as @a[scores={playtime=3000..},advancements={green:creative/sounds_good=false}] run advancement grant @s only green:creative/sounds_good
execute as @a[scores={playtime=18000..},advancements={green:creative/killtime=false}] run advancement grant @s only green:creative/killtime
execute as @a[scores={playtime=30000..},advancements={green:creative/super_dope=false}] run advancement grant @s only green:creative/super_dope
execute as @a[scores={playtime=60000..},advancements={green:creative/really_old=false}] run advancement grant @s only green:creative/really_old
execute as @a[scores={playtime=100..449,flag=0}] run scoreboard players set @s flag 1
execute as @a[scores={playtime=450..,flag=1}] run scoreboard players set @s flag 2

execute in minecraft:green/playground as @a[scores={flag=1..},x=-250,y=65,z=254,distance=..2] run scoreboard players add @s cleaner 1
execute in minecraft:green/playground as @a[scores={flag=1..},x=-250,y=65,z=254,distance=..2] run tellraw @s ["",{"text":"[","color":"dark_gray"},{"text":"控制台","bold":true,"color":"gold"},{"text":"] ","color":"dark_gray"},{"text":"档案馆挂机奖励已发放","color":"aqua"}]
effect give @a[tag=!grant,tag=confirmed,scores={flag=..1}] minecraft:glowing 30 1 true

scoreboard players set @a[scores={flag=3..}] cleaner 85