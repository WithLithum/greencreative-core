# Enables
scoreboard players enable @a[scores={flag=3..}] remove_pillagers
scoreboard players enable @a[scores={flag=3..}] query_player
scoreboard players enable @a[scores={flag=4..}] block
scoreboard players enable @a[scores={flag=3..}] make_mouse
scoreboard players enable @a[scores={flag=3..}] unmouse
scoreboard players enable @a help
scoreboard players enable @a version
scoreboard players enable @a[tag=confirmed] spawn

# Disables
scoreboard players reset @a[scores={flag=..2}] remove_pillagers
scoreboard players reset @a[scores={flag=..2}] query_player
scoreboard players reset @a[scores={flag=..3}] block
scoreboard players reset @a[scores={flag=..2}] make_mouse
scoreboard players reset @a[scores={flag=..2}] unmouse
scoreboard players reset @a[tag=!confirmed] spawn

# 显示队伍
team add adminDisplay "管理员"
team add modDisplay "协管员"
team add mvpDisplay "咸鱼+"
team add mvplessDisplay "咸鱼"
team add nothingDisplay "标准用户"

team modify modDisplay prefix [{"text": "[","color": "white","bold": false},{"text": "MOD","color": "green","bold": true},{"text": "] ","color": "white","bold": false}]
team modify adminDisplay prefix [{"text": "[","color": "red","bold": false},{"text": "ADMIN","color": "white","bold": true},{"text": "] ","color": "red","bold": false}]
team modify mvpDisplay prefix [{"text": "[","color": "dark_aqua","bold": false},{"text": "FISH+","color": "aqua","bold": true},{"text": "] ","color": "dark_aqua","bold": false}]
team modify mvplessDisplay prefix [{"text": "[","color": "dark_blue","bold": false},{"text": "FISH","color": "blue","bold": true},{"text": "] ","color": "dark_blue","bold": false}]

team join adminDisplay @a[scores={flag=4},team=!adminDisplay]
team join modDisplay @a[scores={flag=3},team=!modDisplay]
team join mvpDisplay @a[scores={flag=2},team=!mvpDisplay]
team join mvplessDisplay @a[scores={flag=1},team=!mvplessDisplay]
team join nothingDisplay @a[team=!nothingDisplay,tag=confirmed,scores={flag=0}]
team modify adminDisplay color red
team modify modDisplay color dark_green
team modify mvpDisplay color aqua
team modify mvplessDisplay color blue
team modify nothingDisplay color gray

tag @a[tag=grant] remove banned2
scoreboard players add @a flag 0
scoreboard players set @a[scores={flag=..1},tag=auto2] flag 2
scoreboard players set @a[scores={flag=0},tag=auto1] flag 1
scoreboard players set @a[scores={flag=..2},tag=mod] flag 3

# Unconfirmed User
gamemode adventure @a[tag=!grant,tag=!confirmed]
execute in minecraft:overworld as @a[distance=0..,tag=!confirmed,tag=!grant,tag=!banned,tag=!banned2] run execute in minecraft:green/playground run tp @s -273 64 210 facing -275 64 210

execute in minecraft:overworld run gamemode adventure @a[tag=!grant,x=-269,y=65,z=242,distance=..25,tag=!banned,tag=!rat]
execute in minecraft:overworld run gamemode creative @a[tag=!grant,x=-269,y=65,z=242,distance=25..,tag=!banned,tag=!dangerous,tag=confirmed]

execute in the_end as @a[distance=..0] run execute in minecraft:overworld run kill @s

#replaceitem entity @a[tag=!grant,tag=!noclothes,scores={flag=0}] armor.head black_stained_glass
#replaceitem entity @a[tag=!grant,tag=!noclothes,scores={flag=0}] armor.chest leather_chestplate{display:{color:16777215}}
#replaceitem entity @a[tag=!grant,tag=!noclothes,scores={flag=0}] armor.legs leather_leggings{display:{color:0}}
#replaceitem entity @a[tag=!grant,tag=!noclothes,scores={flag=0}] armor.feet leather_boots{display:{color:0}}
#replaceitem entity @a[tag=!grant,tag=!noclothes,scores={flag=1}] armor.head leather_helmet{display:{color:6783231}}
#replaceitem entity @a[tag=!grant,tag=!noclothes,scores={flag=2}] armor.head leather_helmet{display:{color:2020584}}
#replaceitem entity @a[scores={flag=3..}] armor.head leather_helmet{display:{color:16711680}}