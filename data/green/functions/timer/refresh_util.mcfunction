execute store result score 在线管理员数量 information run execute if entity @a[scores={flag=3..}]
execute store result score 在线玩家数量 information run execute if entity @a[tag=!rat,tag=!banned,tag=!banned2]
execute store result score itemAmount cache run execute if entity @e[type=item]

# Advancements
advancement grant @a[tag=confirmed,advancements={green:creative/root=false}] only green:creative/root

# Spawnpoint
spawnpoint @a[tag=confirmed] -270 65 242